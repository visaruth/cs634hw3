#!/usr/bin/env python
# coding: utf-8

# In[66]:


from matplotlib import pyplot as plt
import cv2 as cv
import numpy as np
from skimage.io import imread
from skimage.measure import regionprops
from skimage.feature import greycomatrix,greycoprops
import math
from skimage.color import rgb2gray
from skimage import img_as_int


# In[126]:


imgg1_1 = imread('./Hw3i2/Ba1.tiff')
imgg1_2 = imread('./Hw3i2/Ba2.tiff')
imgg2_1 = imread('./Hw3i2/Br1.tiff')
imgg2_2 = imread('./Hw3i2/Br2.tiff')
imgg3_1 = rgb2gray(cv.imread('./Hw3i2/Ci1.jpg',cv.IMREAD_GRAYSCALE))
imgg3_2 = rgb2gray(cv.imread('./Hw3i2/Ci2.jpg',cv.IMREAD_GRAYSCALE))
imgg3_3 = rgb2gray(cv.imread('./Hw3i2/Ci3.jpg',cv.IMREAD_GRAYSCALE))
imgg3_4 = rgb2gray(cv.imread('./Hw3i2/Ci4.jpg',cv.IMREAD_GRAYSCALE))
imgg4_1 = rgb2gray(cv.imread('./Hw3i2/Ot1.jpg',cv.IMREAD_GRAYSCALE))
imgg4_2 = rgb2gray(cv.imread('./Hw3i2/Ot2.jpg',cv.IMREAD_GRAYSCALE))
imgg4_3 = rgb2gray(cv.imread('./Hw3i2/Ot3.jpg',cv.IMREAD_GRAYSCALE))
imgg4_4 = rgb2gray(cv.imread('./Hw3i2/Ot4.jpg',cv.IMREAD_GRAYSCALE))
imgg4_5 = rgb2gray(cv.imread('./Hw3i2/Ot5.tiff',cv.IMREAD_GRAYSCALE))


# In[74]:


plt.imshow(imgg3_1)
plt.show()


# In[147]:


result = greycomatrix(imgg4_5, [1], [0, np.pi/4, np.pi/2, 3*np.pi/4],levels=256)
contrast = greycoprops(result, 'contrast')
print(contrast)


# In[148]:


result = greycomatrix(imgg4_5, [1], [0, np.pi/4, np.pi/2, 3*np.pi/4],levels=256)
contrast = greycoprops(result, 'dissimilarity')
print(contrast)


# In[149]:


result = greycomatrix(imgg4_5, [1], [0, np.pi/4, np.pi/2, 3*np.pi/4],levels=256)
contrast = greycoprops(result, 'homogeneity')
print(contrast)


# In[150]:


result = greycomatrix(imgg4_5, [1], [0, np.pi/4, np.pi/2, 3*np.pi/4],levels=256)
contrast = greycoprops(result, 'ASM')
print(contrast)


# In[151]:


result = greycomatrix(imgg4_5, [1], [0, np.pi/4, np.pi/2, 3*np.pi/4],levels=256)
contrast = greycoprops(result, 'energy')
print(contrast)


# In[7]:


imgn0 = imread('./Hw3i3/L-set/n00.bmp')
imgn1 = imread('./Hw3i3/L-set/n01.bmp')
imgn2 = imread('./Hw3i3/L-set/n02.bmp')
imgn3 = imread('./Hw3i3/L-set/n03.bmp')
imgn4 = imread('./Hw3i3/L-set/n04.bmp')
imgn5 = imread('./Hw3i3/L-set/n05.bmp')
imgn6 = imread('./Hw3i3/L-set/n06.bmp')
imgn7 = imread('./Hw3i3/L-set/n07.bmp')
imgn8 = imread('./Hw3i3/L-set/n08.bmp')
imgn9 = imread('./Hw3i3/L-set/n09.bmp')


# In[8]:


plt.imshow(imgn5)
plt.show()


# In[9]:


feature1 = regionprops(imgn9)
feature1[0].euler_number


# In[10]:


feature1 = regionprops(imgn9)
feature1[0].major_axis_length/feature1[0].minor_axis_length


# In[11]:


feature1 = regionprops(imgn9)
feature1[0].solidity


# In[12]:


feature1 = regionprops(imgn9)
(4*math.pi*feature1[0].area)/(math.pow(feature1[0].perimeter, 2))


# In[13]:


feature1 = regionprops(imgn9)
4*(feature1[0].area)/(math.pi*math.pow(feature1[0].major_axis_length, 2))


# In[ ]:




